const db = require('./db/models');

async function main() {
  const address = await db.address.create({
    street: 'Soborniy',
    building: (Math.random() * 100).toFixed(),
    country: 'UA'
  });

  console.log('created address ===>> ', address.toJSON());
  let role;
  try  {
    role = await db.role.create({
      name: 'admin'
    });

  } catch (e) {
    console.error(e);

    role = await db.role.findOne({  where: { name: 'admin' }});
  }

  console.log('found role ===>> ', role.toJSON());

  const user = await db.user.create({
    first_name: 'John',
    last_name: 'Doe',
    email: 'somee@test.com',
    password: '123'
  });
  console.log('created user ===>> ', user.toJSON());

  await user.setAddress(address);

  await user.setRole(role);


  const users = await db.user.findAll({ include: [{ model: db.role }, { model: db.address }] });
  console.log(users);
}


main();

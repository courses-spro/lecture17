'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class address extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      address.hasOne(models.user);

    }
  };
  address.init({
    street: DataTypes.STRING,
    building: DataTypes.INTEGER,
    country: DataTypes.STRING
  }, {
    sequelize,
    underscored: true,
    modelName: 'address',
  });
  return address;
};

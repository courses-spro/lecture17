'use strict';
const {Op} = require('sequelize');
const roles = [{ name: 'admin' }, { name: 'superadmin' }, { name: 'customer' }];
const db = require('../models');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    for (const role of roles) {
      await db.role.create(role);
    }
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    await queryInterface.bulkDelete('roles', {
      name: {
        [Op.in]: roles.map((currentRoleOnIteration) => {return currentRoleOnIteration.name;})
      }
    })

  }
};

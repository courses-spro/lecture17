#!/bin/bash

set -e
while ! (timeout 3 bash -c "</dev/tcp/${DB_HOST}/${DB_PORT}") &> /dev/null;
do
    echo waiting for PostgreSQL to start;
    sleep 3;
done;
echo PostgreSQL is up

npm run migration:start
npm run seeder:start
npm run start
